from mongoengine.django.auth import User
from social_auth.db.mongoengine_models import UserSocialAuth
from app.models import CustomUser
from app.models import Project
UserSocialAuth.objects.all().delete()
CustomUser.objects.all().delete()
User.objects.all().delete()
Project.objects.all().delete()
