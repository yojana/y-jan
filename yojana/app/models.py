# Define a custom User class to work with django-social-auth
from mongoengine import *
from mongoengine.django import auth
from mongoengine import *
import datetime
from mongoengine.fields import GridFSProxy

from yojana.settings import DBNAME
from mongoengine.connection import Connection
from gridfs import GridFS
connection = Connection()
db = connection[DBNAME]
fs = GridFS(db)

from django.template.defaultfilters import slugify

class Project(Document):
    name         = StringField(max_length=255)
    url          = StringField(max_length=255, unique=True)
    contributor  = StringField(max_length=255)
    sources      = ListField(FileField(unique=True))

    @classmethod
    def create_or_update_project(cls, projectname, filename, sourcecode, username):
        extension = None
        if filename.find('.') != -1:
            extension = slugify(str(filename)[str(filename).index('.')+1:])
            filename = str(filename)[:str(filename).index('.')]
        filename = slugify(filename)
        if extension:
            filename = filename+'.'+extension
        user = CustomUser.objects(username=username+'CU').first()
        if user:
            project = Project.objects(name=projectname)
            if project:
                project = project.first()
                sources = project.sources
                remove = None
                for source in sources:
                    if source.filename == filename:
                        # Already existing file
                        remove = source
                if remove:
                    sources.remove(remove)

                newfile = GridFSProxy()
                newfile.put(sourcecode, filename=filename)
                newfile.close()

                sources.insert(0, newfile)
                project.sources = sources
                project.save()

            else:
                newfile = GridFSProxy()
                newlist = []
                newfile.put(sourcecode, filename=filename)
                newfile.close()
                newlist.append(newfile)
                project = cls(name=projectname, url=slugify(projectname), contributor=username, sources=newlist)
                user.projects.insert(0,project)
                project.save()
                user.save()
    @classmethod
    def extractSource(cls, targetid):
        print dir(targetid)
        #sourcefile = fs.get(targetid)
        #sourcecode = sourcefile.read()
        #sourcefile.close()
        sourcecode = targetid.read()
        targetid.close()
        return sourcecode

class CustomUserManager(auth.User):
    @classmethod
    def create_user(cls, username, email, usn=None, experience=1, projects=[]):
        """Create (and save) a new user with the given username, password and email address."""
        user = CustomUser.create_user(username, email, usn, experience, projects)
        return user

class CustomUser(auth.User):
    username      = StringField(max_length=30, required=True)
    first_name    = StringField(max_length=30)
    last_name     = StringField(max_length=30)
    email         = EmailField()
    is_active     = BooleanField(default=True)
    is_superuser  = BooleanField(default=False)
    is_staff      = BooleanField(default=False)
    last_login    = DateTimeField(default=datetime.datetime.now)
    date_joined   = DateTimeField(default=datetime.datetime.now)
    usn           = StringField(max_length=10, unique=True)
    experience    = IntField(min_value=1, max_value=6)
    projects      = ListField(ReferenceField(Project))
    manager       = CustomUserManager()

    meta = {
                'allow_inheritance': True,
                'indexes': [
                    {'fields': ['username'], 'unique': True, 'sparse': True, 'types': False}
            ]
        }

    def is_authenticated():
        return True

    @classmethod
    def create_user(cls, username, email, usn=None, experience=1, projects=[]):
        """Create (and save) a new user with the given username, password and email address."""
        user = cls(username=username+'CU', email=email, usn=usn, experience=experience, projects=projects)
        try:
            existinguser = cls.objects.get(username=username+'CU')
        except:
            user.save()
        return user

    @classmethod
    def update_user(cls, username, usn):
        user = CustomUser.objects(username=username+'CU')
        if len(user) == 0:
            try:
                mongouser = auth.User.objects.get(username=username)
                user = CustomUser.create_user(username=mongouser.username, email=mongouser.email, usn=usn, experience=1, projects=[])
            except Exception as e:
                print e

        else:
            user[0].update(usn=usn)
