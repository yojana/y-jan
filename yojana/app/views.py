from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.messages.api import get_messages

from social_auth import __version__ as version
from social_auth.utils import setting

from models import CustomUser
from models import Project

def root(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return HttpResponseRedirect('/profile/'+request.user.__str__()+'/')
    else:
        return render_to_response('homepage/homepage.html', 
                                  RequestContext(request))

@login_required
def home(request):
    """Login complete view, displays user data"""
    return render_to_response('homepage/homepage.html', RequestContext(request))
    
def welcome(request):
    """For newly associated user, accepted additional info (USN)"""
    return render_to_response('welcome/welcome.html', RequestContext(request))

def usn(request):
    """Registration completion handling"""
    CustomUser.update_user(request.user.__str__(), request.POST['usn'])
    return HttpResponseRedirect('/')

def forum(request):
  """Moot forums"""
  if request.user.is_authenticated():
    return render_to_response('moot/moot.html', RequestContext(request))
  else:
    return HttpResponseRedirect('/')

def dashboard(request, name):
  user = CustomUser.objects(username=name+'CU')
  username = str(user[0].username)
  ctx = {'profile':user[0], 'username':username[:-2], 'numberOfProjects': len(user[0].projects)}
  return render_to_response('lists/profile.html', ctx, RequestContext(request))

def project_dashboard(request, projectname):
  project = Project.objects(url=projectname)
  print project[0].sources
  ctx = {'project':project[0], 'numberOfFiles':len(project[0].sources)}
  return render_to_response('lists/project.html', ctx, RequestContext(request))

def showsource(request, projectname, filename):
  project = Project.objects(url=projectname)
  if project:
    project = project[0]
    for source in project.sources:
      if source.filename == filename:
        targetfile = source
    if targetfile:
      sourcecode = targetfile.read()
      return render_to_response('lists/showsource.html', {'project':project, 'sourcecode':sourcecode}, 
        RequestContext(request))    
  return HttpResponse(404)

def delete(request, projectname, filename):
  project = Project.objects(url=projectname)
  if project:
    project = project[0]
    for source in project.sources:
      try:
        if source.filename == filename:
          targetfile = source
      except AttributeError:
        pass
    if targetfile:
      project.sources.remove(targetfile)
      project.save()
      targetfile.delete()
      return HttpResponseRedirect('/project/'+projectname+'/')
  return HttpResponse(404)

def profiles(request):
    users = CustomUser.objects.all()
    ctx = {'users':users}
    return render_to_response('homepage/profiles.html', ctx, RequestContext(request))

def projects(request):
    projects = Project.objects.all()
    ctx = {'projects':projects}
    return render_to_response('homepage/projects.html', ctx, RequestContext(request))

def upload(request):
    if request.method == 'POST':
        projectname = request.POST['name']
        filename    = request.FILES['file'].name
        sourcecode  = request.FILES['file'].read()
        print 
        if len(projectname) > 0:
            Project.create_or_update_project(projectname, filename, sourcecode, request.user.__str__())
        return HttpResponse(200)
    else:
        return HttpResponse(404)


def error(request):
    """Error view"""
    messages = get_messages(request)
    return render_to_response('error.html', {'version': version,
                                             'messages': messages},
                              RequestContext(request))

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return HttpResponseRedirect('/')
