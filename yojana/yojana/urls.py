from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from app.views import root, home, logout, error, close_login_popup, welcome, usn, forum, dashboard
from app.views import profiles, projects, upload, project_dashboard, showsource, delete

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'YAT.views.home', name='home'),
    # url(r'^YAT/', include('YAT.foo.urls')),
    url(r'^$', root, name='root'),
    url(r'^home/$', home, name='home'),
    url(r'^welcome/$', welcome, name='welcome'),
    url(r'^usn/$', usn, name='usn'),
    url(r'^forum/$', forum, name='forum'),
    url(r'^error/$', error, name='error'),
    url(r'^logout/$', logout, name='logout'),

    url(r'^profile/(?P<name>.+)/$', dashboard, name='dashboard'),
    url(r'^project/(?P<projectname>.+)/(?P<filename>.+)/delete/$', delete, name='delete'),
    url(r'^project/(?P<projectname>.+)/(?P<filename>.+)$', showsource, name='showsource'),
    url(r'^project/(?P<projectname>.+)/$', project_dashboard, name='project_dashboard'),
    url(r'^profiles/$', profiles, name='profiles'),
    url(r'^projects/$', projects, name='projects'),

    url(r'^upload/$', upload, name='upload'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('social_auth.urls')),
)
